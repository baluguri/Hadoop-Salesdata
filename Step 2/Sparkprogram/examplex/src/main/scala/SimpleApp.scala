

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.hive.HiveContext
import java.sql.SQLException

object SimpleApp {
  def main(args: Array[String]) {
    
   
    val conf = new SparkConf().setAppName("Simple Application") //.setMaster("local[*]")
    val sc = new SparkContext(conf)
    val hc = new org.apache.spark.sql.hive.HiveContext(sc)

    val salesDF = hc.sql("select regionid,promotionid,promotionname,promotioncost,weekdaysales,weekendsales from salesbyregion")
    val aggSalesSplitDF = salesDF.groupBy("regionid", "promotionid", "promotioncost").agg(sum("weekdaysales").alias("wkdaysales"), sum("weekendsales").alias("wkendsales"))
    aggSalesSplitDF.registerTempTable("aggTempTable")
    try{
      
        hc.sql("drop table if exists regionsalesp")
    }catch{
      case ex:SQLException => { println("Drop Exception ignored")} 
    }
    
    hc.sql("create external table regionsalesp as Select * from aggTempTable")
    
    val aggSalestotalDF = aggSalesSplitDF.selectExpr("regionid", "promotionid", "promotioncost", "( wkdaysales + wkendsales) as total")
    val regionmax = aggSalestotalDF.groupBy(col("regionid").alias("tregion")).agg(max("total").alias("ttotal"))
    val maxsalesDF = aggSalestotalDF.join(regionmax).where(col("regionid") === col("tregion") and col("total") === col("ttotal")).select("regionid","promotionid","promotioncost","total")
    maxsalesDF.registerTempTable("maxSalesTemp")
    
    try{
         hc.sql("drop table if exists maxsalesp")
    }catch{
      case ex:SQLException => { println("Drop Exception ignored")} 
    }
    
    hc.sql("create external table maxsalesp as Select * from maxSalesTemp")
    
  }
}
      
