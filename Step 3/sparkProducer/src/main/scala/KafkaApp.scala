import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.hive.HiveContext
import java.sql.SQLException
import java.util.Properties
import org.apache.spark.sql.SQLContext._


object KafkaApp {
  def main(args: Array[String]) {
    
   
    val conf = new SparkConf().setAppName("SparkKafka Application")
    val sc = new SparkContext(conf)
    val sqlcon = new SQLContext(sc) 
    val hc = new org.apache.spark.sql.hive.HiveContext(sc)  
    
    
    val salesDF = hc.sql("select regionid,promotionid,promotionname,promotioncost,weekdaysales,weekendsales from salesbyregion")
    val aggSalesSplitDF = salesDF.groupBy("regionid", "promotionid", "promotioncost")
                                 .agg(sum("weekdaysales").alias("wkdaysales"), sum("weekendsales").alias("wkendsales"))
                                 .map( row => row.mkString(",") )
    
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("client.id", "ScalaProducerExample")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
 
    val kfksend = sc.broadcast(KafkaBroadCast(props))
    aggSalesSplitDF.foreach( row => kfksend.value.send("RetailSales",row))
    
    println("Done. Published to Kafka")
   }

 }   
    
      