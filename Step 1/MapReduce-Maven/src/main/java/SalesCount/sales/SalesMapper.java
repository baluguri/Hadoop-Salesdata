package SalesCount.sales;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class SalesMapper extends Mapper<LongWritable, Text, Text, Text>
{
  public SalesMapper() {}
  
  public void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException
  {
    String[] linesplit = value.toString().split(",");
    Text Intermediate_Sales_Rec = new Text();
    Text Salesamt = new Text();
    if (!linesplit[3].equals("0"))
    {
      if ((linesplit[5].equals("Saturday") | linesplit[5].equals("Sunday")))
      {


        Salesamt.set("0.0," + linesplit[4]);
      }
      else
      {
        Salesamt.set(linesplit[4] + "," + "0.0");
      }
      Intermediate_Sales_Rec.set(linesplit[0] + "," + linesplit[3] + "," + linesplit[7] + "," + linesplit[6]);
      context.write(Intermediate_Sales_Rec, Salesamt);
    }
  }
  
  public void run(Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException
  {
    setup(context);
    while (context.nextKeyValue()) {
      map((LongWritable)context.getCurrentKey(), (Text)context.getCurrentValue(), context);
    }
    System.out.println("#############mapping done");
    cleanup(context);
  }
}