package SalesCount.sales;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class SalesReducer
  extends Reducer<Text, Text, Text, Text>
{
  static HashMap<String, String> PromoTable = new HashMap();
  
  public SalesReducer() {}
  
  public void reduce(Text Saleskey, Iterable<Text> SalesValues, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException
  {
    String[] ReqSalesFields = Saleskey.toString().split(",");
    
    Text SalesCount = new Text();
    
    Double WeekDaySales = Double.valueOf(0.0D);
    Double WeekEndSales = Double.valueOf(0.0D);
    
    Text AggSaleskey = new Text();
    
    String[] PromoDetails = ((String)PromoTable.get(ReqSalesFields[1])).split(",");
    

    if (PromoDetails == null)
    {
      AggSaleskey.set(ReqSalesFields[2] + "," + ReqSalesFields[3] + "," + ReqSalesFields[0] + "," + 
        ReqSalesFields[1] + "," + "No Promo Name" + "," + "0.0" + "," + ReqSalesFields[1]);
    } else {
      AggSaleskey.set(ReqSalesFields[2] + "," + ReqSalesFields[3] + "," + ReqSalesFields[0] + "," + 
        ReqSalesFields[1] + "," + PromoDetails[0] + "," + PromoDetails[1] + "," + ReqSalesFields[1]);
    }
    

    for (Text v : SalesValues)
    {
      WeekDaySales = Double.valueOf(WeekDaySales.doubleValue() + Double.parseDouble(v.toString().split(",")[0]));
      WeekEndSales = Double.valueOf(WeekEndSales.doubleValue() + Double.parseDouble(v.toString().split(",")[1]));
    }

    String AggSales = Double.toString(WeekDaySales.doubleValue()) + "," + Double.toString(WeekEndSales.doubleValue());
    
    context.write(AggSaleskey, new Text(AggSales));
  }
  


  public void setup(Reducer<Text, Text, Text, Text>.Context context)
    throws IOException, InterruptedException, NullPointerException
  {
    try
    {
      System.out.println("Inside Reducer");
      URI[] paths = context.getCacheFiles();
      
      if (paths == null) {
        System.out.println("Cache File not added");
      } else {
        System.out.println("Cache Paths:" + paths[0].toString());
        FileSystem fs = FileSystem.get(context.getConfiguration());
        Path hdfscachepath = new Path(paths[0].toString());
        FSDataInputStream cin = fs.open(hdfscachepath);
        String cachemessage = IOUtils.toString(cin, "UTF-8");
        for (String eachrecord : cachemessage.split("\n")) {
          LoadtoHashmap(eachrecord);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println("Promotion Lookup File not loaded");
    }
  }
  

  private void LoadtoHashmap(String eachrecord)
  {
    String[] eachfield = eachrecord.split(",");
    
    PromoTable.put(eachfield[0], new String(eachfield[1] + "," + eachfield[2]));
  }
}