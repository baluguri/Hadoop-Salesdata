import org.apache.hadoop.fs.Path
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapred.TableOutputFormat
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.mapred.JobConf
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.kafka.clients.consumer.KafkaConsumer
import java.util.Properties
import scala.collection.JavaConverters._
import java.util
import scala.collection.mutable.ListBuffer
import org.apache.hadoop.hbase.client.HTable
import org.apache.kafka.clients.consumer.ConsumerConfig

object HBaseWrite {
   
  def main(args: Array[String]) {
    
     val sparkConf = new SparkConf().setAppName("HBaseWrite").setMaster("local")
     val sc = new SparkContext(sparkConf)
     
     //****************set conf for HBASE
     
     val conf = HBaseConfiguration.create()
     val outputTable = "RetailData"
     val hTable = new HTable(conf, outputTable);
     
     
     //*****************setup and create a consumer
     val TOPIC="RetailSales"

     val  props = new Properties()
     props.put("bootstrap.servers", "localhost:9092")
     props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
     props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
     val r = new scala.util.Random
     props.put("group.id", "New"+r.nextString(2))
     props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); 
    
     val consumer = new KafkaConsumer[String, String](props)
    
     consumer.subscribe(util.Collections.singletonList(TOPIC))
     sys addShutdownHook(shutdown(consumer,hTable,sc))
     
     
     while(true){
        
        val records=consumer.poll(100)
        
        for (record<-records.asScala){
          
           val fields:Array[String] = record.value.split(",")
           //Record structure  (Region ID, Promotion ID, Promotion Cost, total weekday sales, total weekend sales)
           
           val p = new Put(Bytes.toBytes(fields(1)))
           p.addColumn(Bytes.toBytes("cf"),Bytes.toBytes("region"), Bytes.toBytes(fields(0)))
           p.addColumn(Bytes.toBytes("cf"),Bytes.toBytes("Promocost"), Bytes.toBytes(fields(2)))
           p.addColumn(Bytes.toBytes("cf"),Bytes.toBytes("WeekdaySales"), Bytes.toBytes(fields(3)))
           p.addColumn(Bytes.toBytes("cf"),Bytes.toBytes("WeekendSales"), Bytes.toBytes(fields(4)))
           hTable.put(p) 
         
        }
        if(records.count > 0){
           println("Records written in this poll:"+ records.count)
           hTable.flushCommits()  
        } 
    }
  
   }
  
   def shutdown(con:KafkaConsumer[String,String],ht:HTable,sc:SparkContext):Unit = {
     println("Closing Consumer..")
     con.commitAsync()
     con.close
     ht.close
     sc.stop()
     
   }
	  
}