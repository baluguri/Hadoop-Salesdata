package SalesCount.sales;

import java.io.PrintStream;
import java.util.Arrays;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

public class SalesDriver extends Configured implements Tool
{
  public SalesDriver() {}
  
  public int run(String[] args) throws Exception
  {
    if (args.length != 6) {
      System.out.println("arglen:" + args.length);
      System.out.println(Arrays.toString(args));
      System.out.println("Usage: [input] [output] [distirbuted] [core-site.xml] [hdfs-site.xml] [mapred-site.xml]");
      System.exit(-1);
    }
    
    Configuration conf = new Configuration();
    
  // conf.addResource(new Path("/home/raghu/Hadoopdir/etc/hadoop/core-site.xml"));
    conf.addResource(new Path(args[3]));
    conf.addResource(new Path(args[4]));
    conf.addResource(new Path(args[5]));
   // conf.set("mapreduce.output.textoutputformat.separator", ",");
    conf.set("mapred.textoutputformat.separator", ";");
    conf.set("mapred.out.textoutputformat.separator", ";");
    
    Path distFilePath = new Path(args[2]);
    
    Job job = Job.getInstance(getConf());
    
    job.addCacheFile(distFilePath.toUri());
   
    job.setJobName("Sales AGG");
    job.setJarByClass(SalesDriver.class);
    
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    
    job.setMapperClass(SalesMapper.class);
    
    job.setReducerClass(SalesReducer.class);
    
    job.setInputFormatClass(TextInputFormat.class);
    job.setOutputFormatClass(TextOutputFormat.class);
    
    Path inputFilePath = new Path(args[0]);
    Path outputFilePath = new Path(args[1]);
 //   FileSystem fs = FileSystem.get(conf);

    FileInputFormat.setInputDirRecursive(job, true);
    FileInputFormat.addInputPath(job, inputFilePath);
    FileOutputFormat.setOutputPath(job, outputFilePath);
    
    FileSystem fs = FileSystem.get(conf);
    

    if (fs.exists(inputFilePath)) {
      System.out.println("Input file  exists");
      System.out.println("-----------------");
    }
    else {
      System.out.println("Input dir doesnt exists");
      System.out.println("-----------------");
    }
    
    if (fs.exists(outputFilePath)) {
      fs.delete(outputFilePath, true);
    } else {
      System.out.println("Output dir doesnt exists");
      System.out.println("-----------------");
    }
    

    if (fs.exists(distFilePath))
    {
      System.out.println("Dist file exists");
      System.out.println("-----------------");
    }
    else
    {
      System.out.println("Dist file not found");
      System.out.println("-----------------");
    }
    
    return job.waitForCompletion(true) ? 0 : 1;
  }
  
  public static void main(String[] args) throws Exception
  {
    SalesDriver sd = new SalesDriver();
    
    int res = org.apache.hadoop.util.ToolRunner.run(sd, args);
    System.exit(res);
  }
}